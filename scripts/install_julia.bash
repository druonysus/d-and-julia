#!/bin/bash
#set -x

VERSION=${1-1.1.0}
SHORTVER=$(awk -F'.' '{print $1"."$2}' <<<$VERSION)
FULLARCH=$(arch)
SHORTARCH=$(test $FULLARCH = x86_64 && echo x64 || echo $FULLARCH)

FILE=julia-${VERSION}-linux-${FULLARCH}.tar.gz
URL="https://julialang-s3.julialang.org/bin/linux/$SHORTARCH/$SHORTVER/$FILE"
TARGET=/usr/local/bin/julia
DESTINATION=/opt/julia-${VERSION}/bin/julia
cd /tmp/
echo "** INFO: Downloading $FILE to /tmp/"
wget --continue --trust-server-names --no-clobber "$URL" || exit 1
cd /opt/
echo "** INFO: Unpacking $FILE into /opt/"
tar -vxzf /tmp/$FILE || exit 2

cd /usr/local/bin/
echo "** INFO: Setting a link to point $TARGET to $DESTINATION"
sudo ln -fs $DESTINATION $TARGET || exit 3
