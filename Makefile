DC=dmd
CC=gcc 
 
CFLAGS=-c -Wall -fPIC
DFLAGS=-shared -fPIC
 
SOURCES=say.d
OBJECTS=$(SOURCES:.d=.o)
 
$(OBJECTS): 
	$(DC) $(DFLAGS) $(SOURCES)
 
lib: $(OBJECTS)
	$(CC) -shared -fPIC -o libsay.so $(OBJECTS)
 
clean:
	rm *.o *.so
