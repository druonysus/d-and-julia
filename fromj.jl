#!/usr/bin/env julia

words = ccall((:say,"./libsay.so"),Cstring,(Cstring,),"Hello from D from Julia")

println(unsafe_string(words))
