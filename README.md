d-and-julia
===
## Intent
I am tring to figure out how to create a shared library written in D, and then use it from within Julia

### Why?
I really like D and Julia, and they both can play nice with C libraries. For D to call out to a C library, a D Interface file is required in place of the C header file. Julia can call into C without such a thing... D allows us to complile into C-compatible object files, so Julia can call into them directly... or at least so goes my understanding. So, I am playing with this.

I would love to evolve this experiment to have D call EFL libraries (Written in C... I don't know if there are Interface files for these already), and then use Julia to call into D. No real reason for any of this other than I like all these tools and I would love to see them work together and document how it could be done -- at least at a very simplified level -- for myself, and perhaps for others. 

## Setup / Known to work with

### openSUSE
This repo was developed and tested locally on [openSUSE Leap 15.1](http://opensuse.org)

### D
The version of D used is provided by the RPMs in the openSUSE repos
  + `dmd-2.085.1`
  + `libphobos2-0_85-2.085.1`
  + `phobos-devel-2.085.1`
  + `phobos-devel-static-2.085.1`

Once you have openSUSE installed, you can get these packages by running:

```
> sudo zypper install dmd phobos-devel-static
```

### Julia
The packages for [Julia](http://julialang.org) in the openSUSE repos are a little old, especially considering how fast Julia is moving right now. I used Julia 1.1 to test this, and I got that right from the upstream-provided tarball. I created [`install_julia.bash`](scripts/install_julia.bash) to make installing this easier.

To run `./scripts/install_julia.bash` just do:

```
> ./scripts/install_julia.bash
```

## Recources

  + [Julia calling C: A minimal example](https://www.juliabloggers.com/julia-calling-c-a-minimal-example/)

## License

The files in this repository are licensed under The Unlicense

See the [LICENSE.txt](LICENSE.txt) file for the specific language and terms of the The Unlicense.
